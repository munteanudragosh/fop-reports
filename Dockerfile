FROM openjdk:8-jre-alpine

WORKDIR /app

COPY ./target/test-0.0.1-SNAPSHOT.jar /app/

# COPY ./src/main/resources/properties/application.properties /app/

ENTRYPOINT ["java","-jar","test-0.0.1-SNAPSHOT.jar"]