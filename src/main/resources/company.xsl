<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format" exclude-result-prefixes="fo">

    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
                    <fo:region-body margin-top="55pt" margin-bottom="60pt"></fo:region-body>
                    <fo:region-before region-name="xsl-region-before" extent="11in"></fo:region-before>
                    <fo:region-after region-name="xsl-region-after" extent="11in" display-align="after"></fo:region-after>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="simpleA4" initial-page-number="1">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block border-bottom="1pt solid black">
                        <fo:inline-container inline-progression-dimension="50%">
                            <fo:block>HEADER TEXT GOES HERE</fo:block>
                            <fo:block font-size="8pt">Date: <xsl:value-of select="companyInfo/localDate"/></fo:block>
                        </fo:inline-container>
                        <fo:inline-container inline-progression-dimension="50%">
                            <fo:block text-align="right">
                                <fo:external-graphic src="classpath:logo.png"/>
                            </fo:block>
                        </fo:inline-container>
                    </fo:block>
                </fo:static-content>

                <fo:static-content flow-name="xsl-region-after">
                    <fo:block border-top="1pt solid black">
                        <fo:inline-container inline-progression-dimension="40%">
                            <fo:block>FOOTER TEXT</fo:block>
                        </fo:inline-container>
                        <fo:inline-container inline-progression-dimension="40%">
                            <fo:block font-family="barcode" font-size="36pt">
                                <xsl:value-of select="companyInfo/barcode"/>
                            </fo:block>
                        </fo:inline-container>
                        <fo:inline-container inline-progression-dimension="20%">
                            <fo:block text-align="right">Page <fo:page-number/></fo:block>
                        </fo:inline-container>
                    </fo:block>
                </fo:static-content>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="16pt" font-weight="bold" space-after="5mm">
                        Company Name: <xsl:value-of select="companyInfo/companyName"/>
                    </fo:block>

                    <fo:block font-size="10pt">
                        <fo:table table-layout="fixed" width="100%" border="1pt solid black">
                            <fo:table-header border="inherit">
                                <fo:table-row border="inherit" background-color="#F2F2F2">
                                    <fo:table-cell border="inherit">
                                        <fo:block font-weight="bold">#</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="inherit">
                                        <fo:block text-align="center" font-weight="bold">Id</fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell border="inherit">
                                        <fo:block text-align="center" font-weight="bold">Name</fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-header>

                            <fo:table-body border="inherit">
                                <xsl:apply-templates select="companyInfo"/>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="companyInfo">
        <xsl:for-each select="employees">
            <fo:table-row border="inherit">
                <fo:table-cell border="inherit">
                    <fo:block>
                        <xsl:value-of select="position()"/>
                    </fo:block>
                </fo:table-cell>

                <fo:table-cell border="inherit">
                    <fo:block text-align="center">
                        <xsl:value-of select="id"/>
                    </fo:block>
                </fo:table-cell>

                <fo:table-cell border="inherit">
                    <fo:block text-align="center">
                        <xsl:value-of select="name"/>
                    </fo:block>
                </fo:table-cell>
            </fo:table-row>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>