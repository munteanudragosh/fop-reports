package com.ing.test.reports;

import com.ing.test.reports.company.CompanyInfo;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.apache.fop.apps.MimeConstants;
import org.apache.fop.configuration.Configuration;
import org.apache.fop.configuration.DefaultConfigurationBuilder;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;

@Component
public class PdfReportGenerator implements ReportGenerator {

    @Override
    public void generateReport(String templateName, String configName, String outputFileName, Object generalInfo) {

        try (InputStream xslStream = PdfReportGenerator.class.getClassLoader().getResourceAsStream(templateName)) {
            URI configUri = PdfReportGenerator.class.getClassLoader().getResource(configName).toURI();
            Source data = getSource(generalInfo);
            Source template = new StreamSource(xslStream);

            DefaultConfigurationBuilder cfgBuilder = new DefaultConfigurationBuilder();
            Configuration cfg = cfgBuilder.buildFromFile(new File(configUri));

            //here we set the factory builder with default base path inside jar (.) and a custom uri resolver
            FopFactoryBuilder fopBuilder = new FopFactoryBuilder(new File(".").toURI(), new ClasspathResolverURIAdapter());
            fopBuilder = fopBuilder.setConfiguration(cfg);
            FopFactory fopFactory = fopBuilder.build();

            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            OutputStream out = new FileOutputStream(outputFileName);

            try {
                // Construct fop with desired output format
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

                // Setup XSLT
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(template);

                // Resulting SAX events (the generated FO) must be piped through to FOP
                Result res = new SAXResult(fop.getDefaultHandler());
                transformer.transform(data, res);
            } catch (TransformerException e) {
                e.printStackTrace();
            } finally {
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private StreamSource getSource(Object generalInfo) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(CompanyInfo.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter sw = new StringWriter();
        Result result = new StreamResult(sw);

        jaxbMarshaller.marshal(generalInfo, result);
        String xmlObject = sw.toString();

        return new StreamSource(new StringReader(xmlObject));
    }
}
