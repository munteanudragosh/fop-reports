package com.ing.test.reports.company;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class CompanyInfo {

    @XmlElement
    private String companyName;

    @XmlElement
    private String barcode;

    @XmlElement
    private String localDate;

    @XmlElement
    private List<Employee> employees;

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }
}
