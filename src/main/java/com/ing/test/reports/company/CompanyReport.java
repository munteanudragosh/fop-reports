package com.ing.test.reports.company;

import com.ing.test.reports.PdfReportGenerator;
import com.ing.test.reports.ReportGenerator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CompanyReport {

    public static void main(String[] args) {
        CompanyReport companyReport = new CompanyReport();
        CompanyInfo companyInfo = companyReport.getGeneralInfo();
        ReportGenerator pdfReportGenerator = new PdfReportGenerator();

        pdfReportGenerator.generateReport("company.xsl", "fop/fop-config.xml", "employee.pdf", companyInfo);
    }

    private CompanyInfo getGeneralInfo() {
        CompanyInfo companyInfo = new CompanyInfo();
        companyInfo.setCompanyName("Company Test Name");
        companyInfo.setBarcode("123456789");
        companyInfo.setLocalDate(LocalDate.now().toString());

        List<Employee> employees = getEmployeesList();
        companyInfo.setEmployees(employees);

        return companyInfo;
    }

    private List<Employee> getEmployeesList() {
        List<Employee> employees = new ArrayList<>();

        for(int i=1; i<70; i++) {
            Employee employee = new Employee();
            employee.setId(i);
            employee.setName("name"+i);

            employees.add(employee);
        }

        return employees;
    }
}
