package com.ing.test.reports;

import org.apache.fop.apps.io.ResourceResolverFactory;
import org.apache.xmlgraphics.io.Resource;
import org.apache.xmlgraphics.io.ResourceResolver;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;

public class ClasspathResolverURIAdapter implements ResourceResolver {

    private final ResourceResolver resourceResolver;

    public ClasspathResolverURIAdapter() {
        this.resourceResolver = ResourceResolverFactory.createDefaultResourceResolver();
    }

    @Override
    public Resource getResource(URI uri) throws IOException {
        if (uri.getScheme().equals("classpath")) {
            URL url = getClass().getClassLoader().getResource(uri.getSchemeSpecificPart());
            return new Resource(url.openStream());
        } else {
            return resourceResolver.getResource(uri);
        }
    }

    @Override
    public OutputStream getOutputStream(URI uri) throws IOException {
        return resourceResolver.getOutputStream(uri);
    }
}
