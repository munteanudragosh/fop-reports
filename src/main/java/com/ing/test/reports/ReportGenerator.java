package com.ing.test.reports;


public interface ReportGenerator {
    void generateReport(String templateName, String configName, String outputFileName, Object generalInfo);
}
