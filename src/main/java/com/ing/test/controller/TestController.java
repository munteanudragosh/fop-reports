package com.ing.test.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {

    @Value("${test.property}")
    private String testProperty;

    @GetMapping(produces = "application/json")
    public Map<String, String> getTestMessage() {
        log.info("logging from test application !!!!!!!!!!!!!!!");

        Map<String, String> result = new HashMap<>();
        result.put("result", "Hello from a test docker application with property: " + testProperty);
        return result;
    }
}
